import requests, json, io

from constants import TOKEN
from entities.entity import Entity

lastResponses = {}

def __getattr__(method):
    def f(**kw):
        url = 'https://api.telegram.org/bot{0}/{1}'.format(TOKEN, method)

        payload = {}
        files = {}

        for key in kw:
            item = kw[key]

            if isinstance(item, io.IOBase):
                files[key] = item.read()
                item.close()
            elif isinstance(item, Entity) or isinstance(item, list):
                payload[key] = json.dumps(item, default = lambda x: x.dict)
            else:
                payload[key] = item

        r = requests.post(url, data = payload, files = files)
        result = json.loads(r.text)

        if method != 'getUpdate':
            lastResponses[method] = result

        return result

    return f

