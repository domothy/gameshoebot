from entities.entity import Entity

class InputMessageContent(Entity):
    def __init__(self, text):
        super().__init__(None)

        self.message_text = text
        self.parse_mode = 'HTML'
        self.disable_web_page_preview = True