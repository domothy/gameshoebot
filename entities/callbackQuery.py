from entities.message import Message
from entities.entity import Entity
from entities.user import User

class CallbackQuery(Entity):
    def __init__(self, query):
        Entity.__init__(self, query)
        if query is None: return

        self.kind = 'callback_query'

        self.id = query.get('id')
        self.user = User(query.get('from'))
        self.message = Message(query.get('message'))

        self.inlineId = query.get('inline_message_id')
        self.chat = query.get('chat_instance')

        self.data = query.get('data')

    @property
    def command(self):
        return self.arguments[0]

    @property
    def arguments(self):
        return self.data.split(' ')
