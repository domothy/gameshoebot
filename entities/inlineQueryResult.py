from random import randint

from entities.entity import Entity
from entities.inlineKeyboardMarkup import InlineKeyboardMarkup
from entities.inputMessageContent import InputMessageContent

class InlineQueryResult(Entity):
    def __init__(self, title, description, text):
        super().__init__(None)

        self.id = randint(0,100000) # Useless field but Telegram returns error if there are duplicate ID results
        self.type = 'article'

        self.input_message_content = InputMessageContent(text)

        self.title = title
        self.description = description

        self.reply_markup = InlineKeyboardMarkup()

    @property
    def keyboard(self):
        return self.reply_markup


