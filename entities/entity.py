class Entity:
    def __init__(self, raw):
        self.raw = raw

    @property
    def dict(self):
        if self.raw is not None:
            return self.raw
        else:
            values = self.__dict__
            values.pop('raw')

            return values

    @property
    def isDefined(self):
        return self.raw is not None

    @property
    def isNone(self):
        return self.raw is None

    def __str__(self):
        return str(self.dict)

    def __repr__(self):
        return str(self.dict)

    def __getattribute__(self, name):
        try:
            return object.__getattribute__(self, name)
        except AttributeError:
            return None

    def serialize(self):
        return self.__dict__