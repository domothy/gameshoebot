from .entity import Entity

class PhotoSize(Entity):
    def __init__(self, photoSize):
        Entity.__init__(self, photoSize)
        if photoSize is None: return

        self.file_id = photoSize.get('file_id')
        self.width = photoSize.get('width')
        self.height = photoSize.get('height')
        self.file_size = photoSize.get('file_size')