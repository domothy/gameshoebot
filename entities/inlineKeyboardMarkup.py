from entities.entity import Entity

class InlineKeyboardMarkup(Entity):
    def __init__(self):
        super().__init__(None)

        self.inline_keyboard = []

    def addButton(self, display, data):
        if not self.rows:
            self.addRow()

        self.inline_keyboard[-1].append({
            'text': display,
            'callback_data': data
        })

    def addRow(self):
        self.inline_keyboard.append([])
