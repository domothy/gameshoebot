import bot

from decorators import private, command

@private
@command('start')
def showWelcomeMessage(message):
    bot.sendMessage(chat_id = message.chat.id,
                    reply_to_message_id = message.id,
                    text = 'I am a bot.\n\n/games - Display games and how to use them\n\n/about - Show information')
    pass

@private
@command('games')
def showGameList(message):
    bot.sendMessage(chat_id = message.chat.id,
                    reply_to_message_id = message.id,
                    text = '<b>Rock paper scissors</b>\n<pre>@gameshoebot rps</pre>\n\n'
                         + '<b>Russian roulette</b>\n<pre>@gameshoebot rr</pre>\n\n'
                         + '<b>Hangman</b>\n<pre>@gameshoebot hangman</pre>\n\n'
                         + '<b>Ouija board</b>\n<pre>@gameshoebot ouija</pre>\n\n'
                         + '<i>Show highscores</i>\n<pre>@gameshoebot highscores</pre>',
                    parse_mode = 'HTML')

@private
@command('about')
def showAbout(message):
    bot.sendMessage(chat_id = message.chat.id,
                    reply_to_message_id = message.id,
                    text = 'Bot made by @domothy\n\nSource code: https://gitlab.com/domothy/gameshoebot',
                    disable_web_page_preview = True)
