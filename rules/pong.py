import threading, random, bot

from decorators import inline, data, callback, command
from entities.inlineKeyboardMarkup import InlineKeyboardMarkup
from entities.inlineQueryResult import InlineQueryResult
from constants import CACHE_TIME

class Game:
    NORMAL, SMASH, MISS = range(1,4)
    DEBUG = False

    def __init__(self, messageId):
        self.lock = threading.Lock()

        self.messageId = messageId
        self.ongoing = True

        self.lastUser = 0
        self.history = []

        self.missRate = 0.05
        self.missMultiplier = 1
        self.smashRate = 0.15

    def tick(self, user):
        if user.hash == self.lastUser: return
        if not self.ongoing: return

        self.lastUser = user.hash

        if random.uniform(0,1) < self.missRate * self.missMultiplier: # Miss
            self.log(user, Game.MISS)
            self.ongoing = False
        else:
            if random.uniform(0,1) < self.smashRate: # Smash
                self.log(user, Game.SMASH)
                self.missMultiplier *= 3
            else:
                self.log(user, Game.NORMAL)
                self.missMultiplier = 1

            if self.missRate < 0.25: self.missRate += 0.01

        self.update()

    def log(self, user, event):
        self.history.append([event, user.first_name])

    def getMessage(self):
        message = "<b>Pong</b>\n\n"

        if Game.DEBUG:
            message += "{0:.2f}% miss | {1} multiplier\n\n".format(self.missRate, self.missMultiplier)

        ping = True

        for entry, username in self.history:
            message += username + " — "

            if entry == Game.MISS:
                message += "<i>miss...</i>\n"

            if entry == Game.SMASH:
                if ping:
                    message += "<b>PING</b>"
                else:
                    message += "<b>PONG</b>"

            if entry == Game.NORMAL:
                if ping:
                    message += "ping"
                else:
                    message += "pong"

            ping = not ping

            message += '\n'

        return message

    def update(self):
        keyboard = InlineKeyboardMarkup()
        keyboard.addButton("Pong" if len(self.history) % 2 == 0 else "Ping", "pong")

        r = bot.editMessageText(inline_message_id = self.messageId,
                              text = self.getMessage(),
                              reply_markup = keyboard if self.ongoing else '{}',
                              parse_mode = 'HTML')
        print(r)

games = {}

@inline
@data('pong')
def inlinePong(query):
    result = InlineQueryResult("Pong",
                               "Pong...",
                               "<b>Pong</b>\n\n<i>Press ping to start</i>")

    result.keyboard.addButton("Ping", "pong")

    bot.answerInlineQuery(inline_query_id = query.id,
                          results = [result],
                          cache_time = CACHE_TIME)

@callback
@command('pong')
def pongNext(query):
    messageId = query.inlineId

    if messageId not in games:
        newGame = Game(messageId)

        games[messageId] = newGame

    game = games[messageId]

    game.lock.acquire()
    try:
        game.tick(query.user)
    finally:
        game.lock.release()

