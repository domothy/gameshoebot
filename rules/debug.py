import bot, json, os

from database import database
from decorators import admin, command, private
from time import sleep
from helper import sanitize

from constants import *

@private
@admin
@command('reboot')
def reboot(message):
    force = False
    if len(message.arguments) > 1:
        if message.arguments[1].lower() == '-f':
            force = True

    text = "Pulling from git..."
    req = bot.sendMessage(chat_id = message.chat.id,
                          text = '<pre>{0}</pre>'.format(text),
                          reply_to_message_id = message.id,
                          parse_mode = 'HTML')

    msgId = req['result']['message_id']

    os.system('cd {0} && git pull > /tmp/message'.format(DIRECTORY))

    with open('/tmp/message','r') as f:
        result = f.read().strip()
        text += '\n\n' +  result

    sleep(1)

    edit(message.chat.id, msgId, text)

    if not force:
        if 'up-to-date' in result.lower(): return
        if 'up to date' in result.lower(): return
        if 'conflict' in result.lower(): return

    text += '\n\nRestarting bot...\n'
    edit(message.chat.id, msgId, text)

    database.set('reboot-message', {'id': msgId, 'chat': message.chat.id, 'text': text})
    database.save()

    os.system('python3.7 {0} rebooted &'.format(DIRECTORY))
    os.system('kill -KILL `pgrep -f gameshoe | head -n 1`')

@private
@admin
@command('kill')
def kill(message):
    bot.sendMessage(chat_id = message.chat.id,
                    text = '<pre>Ok :(</pre>',
                    reply_to_message_id = message.id,
                    parse_mode = 'HTML')

    os.system('kill -KILL `pgrep -f gameshoe | head -n 1`')

@private
@admin
@command('dbdump')
def dbdump(message):
    if len(message.arguments) > 1:
        value = database.get(message.arguments[1])
    else:
        value = database.values

    msg = json.dumps(value, indent=1)
    msg = sanitize(msg)

    bot.sendMessage(chat_id = message.chat.id,
                    text = '<pre>{0}</pre>'.format(msg),
                    reply_to_message_id = message.id,
                    parse_mode = 'HTML')

@private
@admin
@command('dbremove')
def dbremove(message):
    if len(message.arguments) < 2: return

    database.pop(message.arguments[1])

    bot.sendMessage(chat_id = message.chat.id,
                    text = '<pre>Ok</pre>',
                    reply_to_message_id = message.id,
                    parse_mode = 'HTML')

@private
@admin
@command('dbkeys')
def dbkeys(message):
    msg = ''

    for key in database.values:
        msg += key + '\n'

    msg = sanitize(msg)

    bot.sendMessage(chat_id = message.chat.id,
                    text = '<pre>{0}</pre>'.format(msg),
                    reply_to_message_id = message.id,
                    parse_mode = 'HTML')

@private
@admin
@command('dbclean')
def dbclean(message):
    before = database.size()

    database.save()
    after = database.size()
    cleaned = before-after

    bot.sendMessage(chat_id = message.chat.id,
                    text = '<pre>Cleaned {0:,} bits ({1:.1f}%)</pre>'.format(cleaned, 100*cleaned/before),
                    reply_to_message_id = message.id,
                    parse_mode = 'HTML')

@private
@admin
@command('dbsize')
def dbsize(message):
    bot.sendMessage(chat_id = message.chat.id,
                    text = '<pre>{:,}</pre>'.format(database.size()),
                    reply_to_message_id = message.id,
                    parse_mode = 'HTML')
@private
@admin
@command('lastresponse')
def lastResponse(message):
    try:
        method = message.arguments[1]
    except IndexError:
        method = 'last'

    try:
        response = bot.lastResponses[method]
        response = json.dumps(response, indent=1)
    except KeyError:
        response = 'Error'

    bot.sendMessage(chat_id = message.chat.id,
                    reply_to_message_id = message.id,
                    text = '<pre>{0}</pre>'.format(response),
                    parse_mode = 'HTML')

@private
@admin
@command('ping')
def lastResponse(message):
    bot.sendMessage(chat_id = message.chat.id,
                    reply_to_message_id = message.id,
                    text = '<pre>Pong</pre>',
                    parse_mode = 'HTML')

def edit(chatId, msgId, text):
    bot.editMessageText(chat_id = chatId,
                        message_id = msgId,
                        text = '<pre>{0}</pre>'.format(text),
                        parse_mode = 'HTML')

def inform():
    info = database.get('reboot-message')
    if not info: return

    text = info['text'] + 'Done'

    edit(info['chat'], info['id'], text)
