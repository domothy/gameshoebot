import threading, rules, decorators

from entities import Update
from decorators import debug

def handle(rawUpdate):
    update = Update(rawUpdate)

    thread = threading.Thread(target=runRules, args=[update])
    thread.start()

@debug
def runRules(update):
    print(update)

    body = update.body

    for funcName in dir(rules):
        attr = getattr(rules, funcName)

        if isinstance(attr, decorators.rule):
            if attr(body):
                print("Rule match: {0}".format(funcName))
                break

