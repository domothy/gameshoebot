
def sanitize(message):
    message = message.replace('&', '&amp;')
    message = message.replace('<', '&lt;')
    message = message.replace('>', '&gt;')
    message = message.replace('"', '&quot;')

    return message
