import json, traceback, bot

from helper import sanitize

from constants import *

# All this ugliness below has the purpose of making rule-making easier with simple annotations

class rule:
    def __init__(self, func, kind):
        self.func = func
        self.kind = kind

    def __call__(self, entity):
        if entity.kind == self.kind:
            return self.func(entity) is None

        return False

class private(rule):
    def __init__(self, func):
        super().__init__(func, 'message')

class inline(rule):
    def __init__(self, func):
        super().__init__(func, 'inline_query')

class callback(rule):
    def __init__(self, func):
        super().__init__(func, 'callback_query')

def command(commandName):
    def inner(func):
        def f(msg):
            if msg.command == commandName:
                return func(msg)
            return False
        return f

    return inner

def data(value):
    def inner(func):
        def f(query):
            if query.data == value:
                return func(query)
            return False
        return f

    return inner

def admin(func):
    def f(msg):
        if msg.user.id != DOM_ID: return False
        return func(msg)

    return f

def reply(func):
    def f(msg):
        if msg.reply_to_message.isNone: return False

        return func(msg)

    return f

def debug(func):
    def f(update):
        try:
            return func(update)
        except:
            print('Exception raised - Reporting error')
            print(traceback.format_exc())
            error  = json.dumps(update.raw, indent=1)
            error += '\n————————————\n'
            error += traceback.format_exc()

            error = sanitize(error)

            bot.sendMessage(chat_id=DOM_ID,
                            text='<pre>' + error + '</pre>',
                            parse_mode='HTML')

            return False

    return f